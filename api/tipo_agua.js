/*
    Lista apenas os pokemons do tipo agua (type = water)
*/

const data = require('../data')

module.exports = function(req, res){

    let result = "pokemon list"

    //Implementação
    const response = data.filter((pokemon) => pokemon.type.indexOf("Water") !== -1)
    res.json(response)
    
    //Retorno
    res.json(result)
}
